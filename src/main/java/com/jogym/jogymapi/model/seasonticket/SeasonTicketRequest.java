package com.jogym.jogymapi.model.seasonticket;

import com.jogym.jogymapi.enums.TicketType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SeasonTicketRequest {

    @NotNull
    @ApiModelProperty(notes = "구분", required = true)
    @Enumerated(EnumType.STRING)
    private TicketType ticketType;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "정기권명(2~20)", required = true)
    private String ticketName;

    @NotNull
    @Min(0)
    @Max(12)
    @ApiModelProperty(notes = " 최대월", required = true)
    private Integer maxMonth;

    @NotNull
    @ApiModelProperty(notes = "요금", required = true)
    private Double unitPrice;
}
