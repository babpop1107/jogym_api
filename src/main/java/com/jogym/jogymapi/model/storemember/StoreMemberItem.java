package com.jogym.jogymapi.model.storemember;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StoreMemberItem {
    private Long id;

    private LocalDateTime dateCreate;

    private String username;

    private String storeName;

    private String address;

    private Boolean isEnabled;

    private StoreMemberItem(Builder builder) {
        this.id = builder.id;
        this.dateCreate = builder.dateCreate;
        this.username = builder.username;
        this.storeName = builder.storeName;
        this.address = builder.address;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<StoreMemberItem> {

        private final Long id;
        private final LocalDateTime dateCreate;
        private final String username;
        private final String storeName;
        private final String address;
        private final Boolean isEnabled;

        public Builder(StoreMember storeMember) {
            this.id = storeMember.getId();
            this.dateCreate = storeMember.getDateCreate();
            this.username = storeMember.getUsername();
            this.storeName = storeMember.getStoreName();
            this.address = storeMember.getAddress();
            this.isEnabled = storeMember.getIsEnabled();
        }

        @Override
        public StoreMemberItem build() {
            return new StoreMemberItem(this);
        }
    }
}
