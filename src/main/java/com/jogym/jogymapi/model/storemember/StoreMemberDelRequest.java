package com.jogym.jogymapi.model.storemember;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreMemberDelRequest {

    private Boolean isEnabled;
}
