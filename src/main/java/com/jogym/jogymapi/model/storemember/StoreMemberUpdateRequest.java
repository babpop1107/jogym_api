package com.jogym.jogymapi.model.storemember;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class StoreMemberUpdateRequest {

    @ApiModelProperty(notes = "대표자명(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처(13)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @ApiModelProperty(notes = "가맹점명(2~50)", required = true)
    @NotNull
    @Length(min = 2, max = 50)
    private String storeName;

    @ApiModelProperty(notes = "주소(2~100)", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String address;
}
