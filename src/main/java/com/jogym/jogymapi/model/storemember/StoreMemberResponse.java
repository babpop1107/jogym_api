package com.jogym.jogymapi.model.storemember;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StoreMemberResponse {
    private LocalDateTime dateCreate;
    private String username;
    private String name;
    private String phoneNumber;
    private String businessNumber;
    private String storeName;
    private String address;

    private StoreMemberResponse(Builder builder) {
        this.dateCreate = builder.dateCreate;
        this.username = builder.username;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.businessNumber = builder.businessNumber;
        this.storeName = builder.storeName;
        this.address = builder.address;
    }

    public static class Builder implements CommonModelBuilder<StoreMemberResponse> {

        private final LocalDateTime dateCreate;
        private final String username;
        private final String name;
        private final String phoneNumber;
        private final String businessNumber;
        private final String storeName;
        private final String address;

        public Builder(StoreMember storeMember) {
            this.dateCreate = storeMember.getDateCreate();
            this.username = storeMember.getUsername();
            this.name = storeMember.getName();
            this.phoneNumber = storeMember.getPhoneNumber();
            this.businessNumber = storeMember.getBusinessNumber();
            this.storeName = storeMember.getStoreName();
            this.address = storeMember.getAddress();
        }
        @Override
        public StoreMemberResponse build() {
            return new StoreMemberResponse(this);
        }
    }
}
