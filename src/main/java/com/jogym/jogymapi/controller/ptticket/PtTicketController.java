package com.jogym.jogymapi.controller.ptticket;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.entity.TrainerMember;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.ptticket.PtTicketItem;
import com.jogym.jogymapi.model.ptticket.PtTicketRequest;
import com.jogym.jogymapi.model.ptticket.PtTicketUpdateRequest;
import com.jogym.jogymapi.service.storemember.StoreMemberService;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.ptticket.PtTicketService;
import com.jogym.jogymapi.service.trainermember.TrainerMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "PT권 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pt-ticket")
public class PtTicketController {
    private final PtTicketService ptTicketService;
    private final StoreMemberService storeMemberService;
    private final TrainerMemberService trainerMemberService;

    @ApiOperation(value = "PT권 등록")
    @PostMapping("/store-member-id/{storeMemberId}/trainer-member-id/{trainerMemberId}")
    public CommonResult setPtTicket(@PathVariable long storeMemberId, @PathVariable long trainerMemberId, @RequestBody @Valid PtTicketRequest request) {
        // 변수값은 PathVariable로, Request는 RequestBody로 받고 Body를 사용할 땐 알아서 검사해주는 Valid가 따라온다.

        StoreMember storeMember = storeMemberService.setStore(storeMemberId);
        // id를 건네주고 원본을 받아오는 과정

        TrainerMember trainerMember = trainerMemberService.getOriginTrainer(trainerMemberId);
        // id를 건네주고 원본을 받아오는 과정

        ptTicketService.setPtTicket(storeMember, trainerMember, request);
        // 건네받은 아이디와 준비한 자료들을 사용하여 기능을 수행시키는 과정

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult을 주어 성공했다는 문구를 남겨준다.
    }

    @ApiOperation(value = "PT권 리스트")
    @GetMapping("/all")
    public ListResult<PtTicketItem> getPtTicket(@RequestParam(value = "page", required = false, defaultValue = "1")int page) {
        // List에서 사용할 도구명을 작성, RequestParam을 사용해 value값에 page를 넣어주고 필수임을 알려주며 page의 타입은 int라는 것을 알려준다.

        return ResponseService.getListResult(ptTicketService.getPtTicket(page), true);
        // return으로 getListResult을 주고 어느 곳의 기능을 사용할 것인 지 선택하는 과정
    }

    @ApiOperation(value = "PT권 수정")
    @PutMapping("/pt-ticket-id/{ptTicketId}")
    public CommonResult putPtTicket(@PathVariable long ptTicketId, @RequestBody @Valid PtTicketUpdateRequest request) {
        // 기능을 수행하기 위해 자료를 준비하는 과정

        ptTicketService.putPtTicet(ptTicketId, request);
        // 준비한 자료들을 넣어 기능을 수행하는 과정

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult를 주어 성공했다는 문구를 남겨주는 과정
    }

    @ApiOperation(value = "PT권 삭제")
    @PutMapping("/id/{id}")
    public CommonResult delPtTicket(@PathVariable long id) {
        // 기능을 수행하기 위해 자료를 준비하는 과정

        ptTicketService.delPtTicket(id);
        // 준비한 자료들을 기능 수행을 위해 사용하는 과정

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult를 주어 성공했다는 문구를 남겨주는 과정
    }
}
