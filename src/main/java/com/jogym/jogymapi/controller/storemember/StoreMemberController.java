package com.jogym.jogymapi.controller.storemember;

import com.jogym.jogymapi.enums.MemberGroup;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.common.SingleResult;
import com.jogym.jogymapi.model.storemember.*;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.storemember.StoreMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "가맹점 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/store-member")
public class StoreMemberController {
    private final StoreMemberService storeMemberService;

    @ApiOperation(value = "가맹점 정보 등록")
    @PostMapping("/data")
    public CommonResult setStoreMember( @RequestBody @Valid StoreMemberCreateRequest request) {
        storeMemberService.setStoreMember(request.getMemberGroup(), request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 비밀번호 수정")
    @PutMapping("/password-change/store-member-id/{storeMemberId}")
    public CommonResult putPassword(@PathVariable long storeMemberId, @RequestBody @Valid StoreMemberUpdatePasswordRequest request) {
        storeMemberService.putPassword(storeMemberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 정보 리스트")
    @GetMapping("/list")
    public ListResult<StoreMemberItem> getStoreMember(@RequestParam(value = "page", required = false, defaultValue = "1")int page) {
        return ResponseService.getListResult(storeMemberService.getStoreMember(page), true);
    }

    @ApiOperation(value = "가맹점 정보 리스트 상세보기")
    @GetMapping("/list-detail")
    public SingleResult<StoreMemberResponse> getStoreMemberDetail(@RequestParam(value = "id")long id) {
        return ResponseService.getSingleResult(storeMemberService.getStoreMemberDetail(id));
    }

    @ApiOperation(value = "가맹점 정보 수정")
    @PutMapping("store-member-change/store-member-id/{storeMemberId}")
    public CommonResult putStoreMember(@PathVariable long storeMemberId, @RequestBody @Valid StoreMemberUpdateRequest request) {
        storeMemberService.putStoreMember(storeMemberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 정보 삭제")
    @PutMapping("/store-member-del/store-member-id/{storeMemberId}")
    public CommonResult putStoreMemberDel(@PathVariable long storeMemberId) {
        storeMemberService.putStoreMemberDel(storeMemberId);
        return ResponseService.getSuccessResult();
    }
}
