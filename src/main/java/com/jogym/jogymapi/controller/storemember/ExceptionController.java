package com.jogym.jogymapi.controller.storemember;

import com.jogym.jogymapi.exception.CAccessDeniedException;
import com.jogym.jogymapi.exception.CAuthenticationEntryPointException;
import com.jogym.jogymapi.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException();
    }
}
