package com.jogym.jogymapi.service.storemember;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.MemberGroup;
import com.jogym.jogymapi.exception.CDuplicateIdExistException;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.exception.CNotMatchPasswordException;
import com.jogym.jogymapi.exception.CNotValidIdException;
import com.jogym.jogymapi.lib.CommonCheck;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.storemember.MemberItem;
import com.jogym.jogymapi.model.storemember.StoreMemberCreateRequest;
import com.jogym.jogymapi.repository.MemberRepository;
import com.jogym.jogymapi.repository.StoreMemberRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final StoreMemberRepository storeMemberRepository;
    private final PasswordEncoder passwordEncoder;
    private final MemberRepository memberRepository;
    
    public Member getMember(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }




    private String makeRandomPassword() {
        Random random = new Random(); // 랜덤 숫자 생성
        int createNum = 0; // 하나의 숫자를 만들어서 기본값 0으로 초기화 / createNum은 한 자리의 랜덤 숫자를 만듦
        String ranNum = ""; // 랜덤 숫자가 들어갈 자리 생성
        String resultNum = ""; // 결과 숫자가 들어갈 자리 생성

        for (int i=0; i<8; i++) { // int 타입으로 줘서 i=0는 초기값으로 설정하고 i<6 은 6번 돈다는 걸 의미하고 i++ 초기값으로 설정된 사이클 횟수를 늘려준다.
            // 얘가 6자리를 만들기 위해선 하나의 랜덤숫자를 6번 반복해야됨
            createNum = random.nextInt(9); // 0부터 9까지의 랜덤 숫자를 생성함 for문이니까 반복
            ranNum = Integer.toString(createNum); // 나온 int타입의 랜덤 숫자를 String 타입으로 줘서 문자로 바꿈
            // 6자리로 랜덤숫자를 만들어야 하는데 숫자끼리 더하면 6자리가 나오지 않고 문자로 바꿔야 6자리로 나열할 수 있기 때문에 int타입을 String 타입으로 변환한다. 1 + 2 = 3 / "1"+"2" = 12
            resultNum += ranNum; // 결과 값은 랜덤으로 나온 숫자 6개를 전부 더한 값이다.
        }
        return resultNum;
    }


    public ListResult<MemberItem> getMembers(int page) {
        Page<StoreMember> originList = storeMemberRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<MemberItem> result = new LinkedList<>();
        for (StoreMember storeMember : originList) {
            result.add(new MemberItem.Builder(storeMember).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }


    public void setFirstMember() {
        String username = "superadmin";
        String password = "idjh195233";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            StoreMemberCreateRequest createRequest = new StoreMemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고관리자");
            createRequest.setPhoneNumber("010-0000-0000");
            createRequest.setBusineesNumber("000-00-00000");
            createRequest.setStoreName("JO GYM");
            createRequest.setAddress("중앙동");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }
    }


    public StoreMember setMember(MemberGroup memberGroup, StoreMemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CNotValidIdException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CNotMatchPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CDuplicateIdExistException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        StoreMember storeMember = new StoreMember.StoreMemberBuilder(createRequest).build();
        return storeMemberRepository.save(storeMember);
    }

    private boolean isNewUsername(String username) {
        long dupCount = storeMemberRepository.countByUsername(username);
        return dupCount <= 0;
    }

}
