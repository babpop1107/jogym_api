package com.jogym.jogymapi.service.member;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.member.MemberItem;
import com.jogym.jogymapi.model.member.MemberRequest;
import com.jogym.jogymapi.model.member.MemberResponse;
import com.jogym.jogymapi.model.member.MemberUpdateRequest;
import com.jogym.jogymapi.repository.MemberRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    // 컨트롤러에서 안되서 주석처리를 해놓았음.
    // 예상하기로는 서비스에서는 문제가 없음.
    // 컨트롤러에서 해결이 되지않아 버려두고 다른거부터 하였음.
    public void setMember(StoreMember storeMember, MemberRequest request) {
        Member member = new Member.MemberBuilder(storeMember, request).build();

        memberRepository.save(member);
    }

    public ListResult<MemberItem> getMemberList(StoreMember storeMember, int page) {
        Page<Member> originList = memberRepository.findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(storeMember, true,  ListConvertService.getPageable(page));
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList.getContent()) {
            result.add(new MemberItem.Builder(member).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }


    public MemberResponse getMember(long memberId, StoreMember storeMember) {
        Member member = memberRepository.findByIdAndStoreMember(memberId,storeMember).orElseThrow(CMissingDataException::new); // 데이터가 없습니다.
        if(!member.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다.
        return new MemberResponse.Builder(member).build();
    }

    public void putMember(long memberId, StoreMember storeMember, MemberUpdateRequest request) {
        Member originData = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다.
        if(originData.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다.
        originData.putMember(request);
        memberRepository.save(originData);
    }

    public void putMemberDelete(long memberId, StoreMember storeMember) {
        Member originData = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다.
        if(originData.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다.
        originData.putMemberDelete();
        memberRepository.save(originData);
    }
}
