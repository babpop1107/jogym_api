package com.jogym.jogymapi.service.seasonticketusedhistory;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.SeasonTicketBuyHistory;
import com.jogym.jogymapi.entity.SeasonTicketUsedHistory;
import com.jogym.jogymapi.exception.CAlreadyDateCheckTodayException;
import com.jogym.jogymapi.exception.CAlreadyUsingSeasonTicketException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.seasonticketusedhistory.SeasonTicketUsedHistoryItem;
import com.jogym.jogymapi.repository.SeasonTicketUsedHistoryRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SeasonTicketUsedHistoryService {
    private final SeasonTicketUsedHistoryRepository seasonTicketUsedHistoryRepository;

    public void setSeasonTicketUsedHistory(SeasonTicketBuyHistory seasonTicketBuyHistory) {
        if (!checkToday(seasonTicketBuyHistory.getMember())) throw new CAlreadyDateCheckTodayException();
        SeasonTicketUsedHistory seasonTicketUsedHistory = new SeasonTicketUsedHistory.SeasonTicketUsedHistoryBuilder(seasonTicketBuyHistory).build();

        seasonTicketUsedHistoryRepository.save(seasonTicketUsedHistory);
    }

    public ListResult<SeasonTicketUsedHistoryItem> getData(int page, Member member) {
        Page<SeasonTicketUsedHistory> originList = seasonTicketUsedHistoryRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<SeasonTicketUsedHistoryItem> result = new LinkedList<>();
        for (SeasonTicketUsedHistory seasonTicketUsedHistory : originList) {
            result.add(new SeasonTicketUsedHistoryItem.Builder(member, seasonTicketUsedHistory).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private boolean checkToday(Member member){
        Optional<SeasonTicketUsedHistory> check = seasonTicketUsedHistoryRepository.findBySeasonTicketBuyHistory_MemberAndDateLast(member, LocalDate.now());

        if (check.isPresent()) return false;
        return true;
    }
}
