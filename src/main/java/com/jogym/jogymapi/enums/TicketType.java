package com.jogym.jogymapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TicketType {
    MONTH("정기권"),
    DAY("일일권");

    private final String name;
}

