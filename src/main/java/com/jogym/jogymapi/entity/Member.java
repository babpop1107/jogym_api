package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.enums.Gender;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.model.member.MemberRequest;
import com.jogym.jogymapi.model.member.MemberUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeMemberId", nullable = false)
    private StoreMember storeMember;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 연락처
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 성별
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;

    public void putMember(MemberUpdateRequest request) {
        this.name = request.getName();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.memo = request.getMemo();
    }

    public void putMemberDelete() {
        this.isEnabled = false;
    }


    private Member(MemberBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.storeMember = builder.storeMember;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.isEnabled = builder.isEnabled;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final LocalDateTime dateCreate;
        private final StoreMember storeMember;
        private final String name;
        private final String phoneNumber;
        private final String address;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final Boolean isEnabled;

        public MemberBuilder(StoreMember storeMember, MemberRequest request) {
            this.dateCreate = LocalDateTime.now();
            this.storeMember = storeMember;
            this.name = request.getName();
            this.phoneNumber = request.getPhoneNumber();
            this.address = request.getAddress();
            this.gender = request.getGender();
            this.dateBirth = request.getDateBirth();
            this.isEnabled = true;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
