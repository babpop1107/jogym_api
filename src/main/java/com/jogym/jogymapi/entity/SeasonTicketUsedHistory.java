package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SeasonTicketUsedHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 최근방문일
    private LocalDate dateLast;

    // 정기권 구매내역 id
    @JoinColumn(name = "seasonTicketBuyHistoryId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private SeasonTicketBuyHistory seasonTicketBuyHistory;

    private SeasonTicketUsedHistory(SeasonTicketUsedHistoryBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.seasonTicketBuyHistory = builder.seasonTicketBuyHistory;
    }

    public static class SeasonTicketUsedHistoryBuilder implements CommonModelBuilder<SeasonTicketUsedHistory> {

        private final LocalDateTime dateCreate;
        private final SeasonTicketBuyHistory seasonTicketBuyHistory;
        private final LocalDate dateLast;

        public SeasonTicketUsedHistoryBuilder(SeasonTicketBuyHistory seasonTicketBuyHistory) {
            this.dateCreate = LocalDateTime.now();
            this.seasonTicketBuyHistory = seasonTicketBuyHistory;
            this.dateLast = LocalDate.now();
        }

        @Override
        public SeasonTicketUsedHistory build() {
            return new SeasonTicketUsedHistory(this);
        }
    }
}
