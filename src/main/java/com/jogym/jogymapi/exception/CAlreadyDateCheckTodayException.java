package com.jogym.jogymapi.exception;

public class CAlreadyDateCheckTodayException extends RuntimeException {
    public CAlreadyDateCheckTodayException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyDateCheckTodayException(String msg) {
        super(msg);
    }

    public CAlreadyDateCheckTodayException() {
        super();
    }
}
