package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Page<Member> findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(StoreMember storeMember,Boolean isEnable, Pageable pageable);
    Optional<Member> findByIdAndStoreMember(Long memberId, StoreMember storeMember);
}
