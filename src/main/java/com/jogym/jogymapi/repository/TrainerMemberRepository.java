package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.entity.TrainerMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TrainerMemberRepository extends JpaRepository<TrainerMember, Long> {
    Page<TrainerMember> findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(StoreMember storeMember, Boolean isEnable, Pageable pageable);
    Optional<TrainerMember> findByIdAndStoreMember(Long id, StoreMember storeMember);
}
