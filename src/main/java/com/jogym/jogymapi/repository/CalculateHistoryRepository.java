package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.CalculateHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CalculateHistoryRepository extends JpaRepository<CalculateHistory, Long> {
}
